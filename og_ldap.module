<?php
/**
 * @file
 * Map LDAP DNs or RDNs to Organic groups.
 *
 * @author Kale Worsley kale@egressive.com
 */

/**
 * Implementation of hook_perm().
 */
function og_ldap_perm() {
  return array('administer og ldap');
}

/**
 * Implementation of hook_menu().
 */
function og_ldap_menu() {
  $items['admin/og/og_ldap'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('og_ldap_admin'),
    'access arguments' => array('administer og ldap'),
    'title' => 'OG LDAP Integration',
  );
  return $items;
}

/**
 * Return an array of groups for an account
 */
function _og_ldap_get_groups($account) {
  global $_ldapauth_ldap;
  
  $attr_name = variable_get('og_ldap_attribute', 'description');
  $attr_value = variable_get('og_ldap_value', 'DrupalGroups');
  
  $groups = _ldapgroups_detect_groups($account);
  
  if (!$groups) {
    return FALSE;
  }
  
  $og_groups = array();
  if (!$_ldapauth_ldap->connection) {
    if (!$_ldapauth_ldap->connect() || !$_ldapauth_ldap->connection) {
      return FALSE;
    }
  }

  $ogmap = og_ldap_og_map();
  if (is_array($ogmap)) {
    foreach ($groups as $group) {
      foreach ($ogmap as $map) {
        if (strstr($group, $map['ldap'])) {
          $og_groups[] = $map;
        }
      }
    }
  }

  foreach ($groups as $group) {
    $attributes = $_ldapauth_ldap->retrieveAttributes($group);
    if (!isset($attributes[$attr_name])) {
      continue;
    }
    // Find if the group is a drupal group
    foreach ($attributes[$attr_name] as $item) {
      if (strstr($item, $attr_value) != FALSE) {
        $og_groups[] = array('og' => _og_cn_name($group), 'ldap' => $group);
      }
    }
  }
  
  return $og_groups;
}

/**
 * Helper function: if group doesn't exist, add it
 */
function _og_ldap_add_group($group) {
  $group_name = $group['og'];
  $result = db_result(db_query("SELECT n.title FROM {node} n WHERE n.type = '%s' AND n.title = '%s'", variable_get('og_ldap_group_type', 'group'), $group_name));
  if (!$result) {
    $node = new stdClass();
    $node->type = variable_get('og_ldap_group_type', 'group');
    $node->uid = 1;
    $node->status = TRUE;
    $node->title = $group_name;

    /* OG stuff */
    $node->og_public = TRUE;
    $node->og_register = FALSE;
    $node->og_directory = TRUE;
    $node->og_description = $group_name;
    $node->og_website = '';
    $node->og_selective = OG_CLOSED;

    node_save($node);
  }
}

/*
 * Syncronise the Groups a user is in with LDAP
 */
function _og_ldap_update_user($account) {
  /* Get a list of existing OG nodes */
  $result = db_query("SELECT n.title FROM {node} n WHERE n.type = '%s'", variable_get('og_ldap_group_type', 'group'));
  $all_groups = array();
  while ($node = db_fetch_object($result)) {
    $all_groups[] = $node->title;
  }

  /* Get a list of groups the user is in from OG */
  $subs = og_get_subscriptions($account->uid);

  /* Find a list of groups the user is in from LDAP */
  $groups = _og_ldap_get_groups($account);

  if ($groups) {
    /* Create any non-existant groups and add the user to all groups */
    foreach ($groups as $group) {
      $group_name = $group['og'];
      /* Create the group if it doesn't already exist */
      if (!in_array($group_name, $all_groups)) {
        _og_ldap_add_group($group);
      }
      
      /* Add the user to the OG group */
      $sql = "SELECT n.nid FROM {node} n, {og} og WHERE n.nid = og.nid AND n.title = '%s' LIMIT 1";
      $result = db_query($sql, $group_name);
      $row = db_fetch_array($result);
      if ($row) {
        $gid = $row['nid'];
        og_save_subscription($gid, $account->uid, array('is_active' => TRUE));
        /* Remove the gid from the subs array. The groups left will be unsubscribed from */
        if (isset($subs[$gid])) {
          unset($subs[$gid]);
        }
      }
    }
  }
  
  /* Remove the user from groups when they are in OG but not in LDAP */
  if (variable_get('og_ldap_ldap_only', 0)) {
    foreach ($subs as $sub) {
      og_delete_subscription($sub['nid'], $sub['uid']);
    }
  }
}

/**
 * Implementation of hook_user().
 */
function og_ldap_user($op, &$edit, &$account, $category = NULL) {
  static $og_done = FALSE; // Used to only add users to OG once per load
  if ($op == 'login' && $og_done == FALSE) {
    $og_done = TRUE;
    // Don't touch users who are not LDAP authenticated
    if (!isset($account->ldap_authentified)) {
      return;
    }
    if (!$account->ldap_authentified) {
      return;
    }
    _og_ldap_update_user($account);
  }
}

/*
 * Create the OG LDAP admin form
 */
function og_ldap_admin() {
  $form['attr_value'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attribute - Value method'),
    '#description' => t('This method lets you define an attribute - value pair that will be used to map LDAP groups to Organic Groups. The first CN value will be used as the Organic Group name.'),
  );
  $form['attr_value']['og_ldap_attribute'] = array(
    '#type' => 'textfield',
    '#title' => t('Attribute'),
    '#description' => t('The attribute used to mark Organic Groups groups in LDAP'),
    '#default_value' => variable_get('og_ldap_attribute', 'description'),
  );
  $form['attr_value']['og_ldap_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value used to mark Organic Groups groups in LDAP'),
    '#default_value' => variable_get('og_ldap_value', 'DrupalGroups'),
  );

  $form['map'] = array(
   '#type' => 'fieldset',
   '#title' => t('Mapping Method'),
   '#description' => t('This method lets you make a direct mapping between LDAP groups and Organic Group.'),
  );
  $form['map']['og_ldap_groups'] = array(
    '#type' => 'textarea',
    '#title' => t('Mapping of LDAP groups to Organic Groups'),
    '#default_value' => variable_get('og_ldap_groups', ''),
    '#description' => t('Enter a list of LDAP groups and their Organic Group mappings, one per line with a | delimiter. Should be in the form [ldap group]|[organic group title].'),
  );
  $form['map']['eg'] = array(
    '#type' => 'markup',
    '#value' => t('<blockquote>ou=National,DC=intranet,DC=example,DC=com|National</blockquote>
<p>or</p>
<blockquote>cn=Accounts,ou=National,DC=intranet,DC=example,DC=com|National - Accounts</blockquote><p>Both examples will match the \'National\' organic group, but only the second one will match \'National - Accounts\'.</p><p>Don\'t forget to fill out the \'Group by attribute\' on the LDAP integration \'Groups\' !link.</p>', array('!link' => l('module settings page', 'admin/settings/ldap/ldapgroups'))),
  );
  $form['og_ldap_ldap_only'] = array(
   '#type' => 'checkbox',
   '#title' => t('LDAP members only'),
   '#default_value' => variable_get('og_ldap_ldap_only', 0),
   '#description' => t('Should members of an organic group that are not a member of a corresponding LDAP group be removed?'),
  );
  $nodetypes = og_get_types('group');
  $options = array();

  foreach ($nodetypes as $value) {
    $options[$value] = node_get_types('name', array('type' => $value));
  }

  $form['og_ldap_group_type'] = array(
    '#title' => 'Organic Group type',
    '#options' => $options,
    '#type' => 'select',
    '#required' => TRUE,
    '#default_value' => variable_get('og_ldap_group_type', 'group'),
    '#description' => t('The organic group content type used for LDAP groups.'),
  );

  return system_settings_form($form);
}

/**
 * Submit handler
 */
/*
function og_ldap_admin_submit($form, &$form_state) {
  variable_set('og_ldap_attribute', $form_state['values']['group_attribute']);
  variable_set('og_ldap_value', $form_state['values']['group_attribute_value']);
  variable_set('og_ldap_ldap_only', $form_state['values']['ldap_only']);
  variable_set('og_ldap_groups', $form_state['values']['groups']);
  variable_set('og_ldap_group_type', $form_state['values']['group_type']);
  drupal_set_message(t('The group attributes have been updated'));
  $form_state['redirect'] = 'admin/og/og_ldap';
}

*/
/**
 * Parse the group strings and return an array of og groups and the matching ldap group
 */
function og_ldap_og_map() {
  $array = array();
  $str = variable_get('og_ldap_groups', '');
  $lines = explode("\n", $str);
  foreach ($lines as $line) {
    $chunks = explode('|', trim($line));
    $array[] = array('ldap' => trim($chunks[0]), 'og' => trim($chunks[1]));
  }
  return $array;
}

/**
 * Get the cn from a group string
 */
function _og_cn_name($group) {
  $cn = explode(',', $group);
  if (substr($cn[0], 0, 2) == 'cn') {
    $group_name = explode('=', $cn[0]);
    $group_name = $group_name[1];
  }
  return $group_name;
}